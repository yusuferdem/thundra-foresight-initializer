from bitbucket_pipes_toolkit import Pipe, get_logger, os, glob

logger = get_logger()

schema = {
  'NAME': {'type': 'string', 'required': True},
  'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}





class DemoPipe(Pipe):
    def run(self):
        super().run()




        logger.info('List of FILES')

        pomFileList = glob.glob('pom.xml')
        gradleList = glob.glob('build.gradle')

        print("list of pom and gradles")
        print(pomFileList)
        print("\n")
        print(gradleList)
        logger.info('END OF LIST FILES')
        logger.info('Executing the pipe...')

        cwd = os.getcwd()
        name = self.get_variable('NAME')

        print(name)
        print(cwd)

        for k, v in sorted(os.environ.items()):
            print(k+':', v)
        print('\n')
        # list elements in path environment variable
        [print(item) for item in os.environ['PATH'].split(';')]

        print("Environment Variables:")
        print('\n')
        print(os.environ)
        self.success(message="Success!")


if __name__ == '__main__':
    pipe = DemoPipe(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
